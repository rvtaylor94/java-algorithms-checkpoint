package com.galvanize;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class Algorithm {

    public boolean allEqual(String data) {

        if (data.equals("")) {
            return false;
        }

        char letter = data.toLowerCase().charAt(0);

        for (int i = 1; i < data.length(); i++) {
            if (data.toLowerCase().charAt(i) != letter) {
                return false;
            }
        }
        return true;
    }

    public Map<String, Long> letterCount(String data) {
        Map<String, Long> result = new HashMap<>();

        for (int i = 0; i < data.length(); i++) {
            String currentLetter = Character.toString(data.charAt(i)).toLowerCase();
            if (result.containsKey(currentLetter)) {
                result.put(currentLetter, result.get(currentLetter) + 1);
            } else {
                result.put(currentLetter, 1L);
            }
        }
        return result;
    }

    public String interleave(List<String> list1, List<String> list2) {
        StringBuilder result = new StringBuilder();

        for (int i = 0; i < list1.size(); i++) {
            result.append(list1.get(i));
            result.append(list2.get(i));
        }

        return result.toString();
    }
}
