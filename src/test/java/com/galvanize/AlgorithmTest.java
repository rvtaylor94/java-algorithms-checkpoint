package com.galvanize;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;

import static org.junit.jupiter.api.Assertions.*;

class AlgorithmTest {

    Algorithm algo;

    @BeforeEach
    void setUp() {
        algo = new Algorithm();
    }

    @Test
    void everyStringIsSame() {
        assertTrue(algo.allEqual("aAa"));
        assertTrue(algo.allEqual("a"));
        assertFalse(algo.allEqual("bbBbabbb"));
        assertFalse(algo.allEqual(""));
    }

    @Test
    void letterCount() {
        Map<String, Long> test1 = algo.letterCount("aa");
        long actualLong1 = test1.get("a");
        assertEquals(2L, actualLong1);

        Map<String, Long> test2 = algo.letterCount("abBcd");
        long actualAs = test2.get("a");
        long actualBs = test2.get("b");
        long actualCs = test2.get("c");
        long actualDs = test2.get("d");
        assertEquals(1L, actualAs);
        assertEquals(2L, actualBs);
        assertEquals(1L, actualCs);
        assertEquals(1L, actualDs);

        Map<String, Long> test3 = algo.letterCount("");
        assertFalse(test3.containsKey("a"));
    }

    @Test
    void isStringInterleave() {
        List<String> test1List1 = new ArrayList<String>() {{
            add("a");
            add("b");
            add("c");
        }};
        List<String> test1List2 = new ArrayList<String>() {{
            add("d");
            add("e");
            add("f");
        }};
        assertEquals("adbecf", algo.interleave(test1List1, test1List2));

        List<String> test2List1 = new ArrayList<String>() {{
            add("a");
            add("c");
            add("e");
        }};
        List<String> test2List2 = new ArrayList<String>() {{
            add("b");
            add("d");
            add("f");
        }};
        assertEquals("abcdef", algo.interleave(test2List1, test2List2));

        List<String> test3List1 = new ArrayList<>(Collections.emptyList());
        List<String> test3List2 = new ArrayList<>(Collections.emptyList());
        assertEquals("", algo.interleave(test3List1, test3List2));
    }

}
